# run vic model
# ./main nmax, tmax, rcut, eta, L, order param
make -s clean 
make -s

basefilname="vectorfieldmovieoutput"
indx=1
for i in $(seq 1 1 99)
do
  basefil="${basefilname}_$indx"
  echo "#x #y #vx #vy" > $basefil.dat
  ./main 500 $i  4.0   0.01  50 vectorfield >> $basefil.dat
  gnuplot -e "set xrange [-5:55]; set yrange [-5:55]; set term png; set output '$basefil.png'; plot '$basefil.dat' using 1:2:3:4 with vectors head filled lt 2"
  indx=$(($indx+1))
done 

ffmpeg -f image2 -r 3 -i ${basefilname}_%0d.png -vcodec mpeg4 -y ${basefilname}.mp4
rm ${basefilname}*.png
rm ${basefilname}*.dat

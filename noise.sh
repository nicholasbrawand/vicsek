# run vic model
# ./main nmax, tmax, rcut, eta, L, orderparam
make -s clean 
make -s
echo "#eta #va">noise.dat
for i in $(seq 0 0.1 10)
do
  #          nmax, tmax, rcut, eta, L
  OP=`./main 500   20    1     $i   3.1 orderparam`
  echo $i $OP
done >> noise.dat
gnuplot -e 'set term png; set output "plot.png"; plot "noise.dat" using 1:2'
open plot.png

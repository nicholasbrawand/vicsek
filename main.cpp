#include <iostream>
#include "omp.h"
#include <random>
#include "utils.h"
#include <cmath>
#include "string.h"
#include <cstdlib>

using namespace std;

void add_to_arr1(double arr1[], double arr2[], const int len){
  for(int i=0;i<len;i++){
    arr1[i] += arr2[i];
  }
}

void add_to_arr1(double arr1[], double arr2fac, double arr2[], const int len){
  for(int i=0;i<len;i++){
    arr1[i] += arr2fac*arr2[i];
  }
}

void normalize(double vhat[], const int nmax, const int ndim){
  double norm = 0.0;
  for(int i=0;i<nmax;i++){

    norm = 0.0;
    for(int j=0;j<ndim;j++){
      norm += pow(vhat[i*ndim+j], ndim);
    }
    norm = sqrt(norm);

    if(norm>numeric_limits<double>::epsilon()){
      for(int j=0;j<ndim;j++){
        vhat[i*ndim+j] = vhat[i*ndim+j]/norm;
      }
    }

  }
}

void update_vhat(const double arr1[], const double arr2[], const int nmax, const int ndim, double work2[], const double rcut, double vhat[], const double eta_by_2, double work1[]){
  // store distances in work2
  // work2 will be upper triangular (ignore lower)
  // work2 will not compute distances for atoms i=j (ignore diag)
  // work1 will be used to sum over vhat and then copied over to vhat at end
  
  default_random_engine generator;
  uniform_real_distribution<double> distribution(-eta_by_2, eta_by_2);
  double dist = 0;
  

  // set work1 to zero before sums
  memset(work1, 0.0, nmax*ndim);

  // sum over work1
  // i never = to j
  // loop over all atoms i
  for(int i=0;i<nmax;i++){
    // only loop over atoms j=i to nmax
    for(int j=i+1;j<nmax;j++){

      // calculate dist
      //ndim*i is atom i
      //ndim*j is atom j
      dist = 0.0;
      for(int k=0;k<ndim;k++){
        dist += pow(arr1[i*ndim+k]-arr2[j*ndim+k], ndim);
      }
      dist = sqrt(dist);

      // update velocity for both atoms i, j
      if(rcut>=dist){
        for(int k=0;k<ndim;k++){
          work1[i*ndim+k] += vhat[j*ndim+k];
          work1[j*ndim+k] += vhat[i*ndim+k];
        }
      }

    }
  }

  // update v
  for(int i=0;i<nmax*ndim;i++){
    vhat[i] = work1[i]+vhat[i]; //include existing vhat
  }

  // normalize v
  normalize(vhat, nmax, ndim);

  // add noise
  for(int i=0;i<nmax*ndim;i++){
    vhat[i] += distribution(generator);
  }

  normalize(vhat, nmax, ndim);

}


//void update_vhat(double vhat[], const double work2[], const int nmax, const int ndim, const double eta_by_2){
//  default_random_engine generator;
//  uniform_real_distribution<double> distribution(-eta_by_2, eta_by_2);
//  // i atom index
//  for(int i=0;i<nmax;i++){
//    // j atom index
//    for(int j=i;j<nmax;j++){
//      // also include self velocity
//      if((work2[i*nmax+j]>0) || (i==j)){
//        // k element index
//        for(int k=0;k<ndim;k++){
//          vhat[i*nmax+k] += vhat[j*nmax+k] + distribution(generator);
//        }
//      }
//    }
//  }
//}

int main(int argc, char *argv[]){
  /*args:
   *  nmax, tmax, rcut, eta, L
   * */

  // init vars
  const int nmax = atoi(argv[1]);
  const int tmax = atoi(argv[2]); // steps not time
  const int ndim = 2;
  const double rcut = atof(argv[3]);
  const double eta = atof(argv[4]);
  const double vo = 2.0;
  const double dt = 0.1;
  const int totdim = nmax*ndim;
  double lbox = atof(argv[5]);
  double va = 0.0;
  const string plot_type = argv[6]; 

  // allocate arrs on heap
  double * pos = new double[totdim]; 
  double * vhat = new double [totdim]; 
  double * work1 = new double [totdim]; 
  double * work2 = new double [nmax*nmax]; 
  memset( pos, 0, totdim*sizeof(double) );
  memset( vhat, 0, totdim*sizeof(double) );
  memset( work1, 0, totdim*sizeof(double) );
  memset( work2, 0, nmax*nmax*sizeof(double) );

  // derived vars
  const double time_max = tmax*dt;
  const double eta_by_2 = eta/2.0;
  const double dt_vo = dt*vo;

  // init arrays
  default_random_engine generator;
  uniform_real_distribution<double> pos_dist(0, lbox);
  uniform_real_distribution<double> vhat_dist(-1, 1);

  for(int i=0;i<totdim;i++){
    pos[i] = pos_dist(generator);
    vhat[i] = vhat_dist(generator);
  }

  // normalize vhat
  normalize(vhat, nmax, ndim);

  // main loop
  for(int t=0;t<tmax;t++){

    // x = x + dt*vo*vhat
    add_to_arr1(pos, dt_vo, vhat, totdim);

    // vhat = <vhat> + [-eta/2,eta/2]
    update_vhat(pos, pos, nmax, ndim, work2, rcut, vhat, eta_by_2, work1);
    // print_arr(pos, nmax, ndim);
    
    // translate pos
    for(int n=0;n<nmax;n++){
      for(int i=0;i<ndim;i++){
        if(abs(pos[n*ndim+i]) > lbox){
          pos[n*ndim+i] =  pos[n*ndim+i] - lbox * int(lbox)/int(pos[n*ndim+i]);
        }else if(pos[n*ndim+i] < 0.0){
          pos[n*ndim+i] =  pos[n*ndim+i] + lbox; // assume not less than -lbox
	}
      }
    }

  }

  
  // plot stuff
  if(plot_type == "orderparam"){
    // this will mess up v[0] and below plots will reflect that
    // va = |sum_a vhat_a|/nmax
    for(int i=1;i<nmax;i++){
      for(int k=0;k<ndim;k++){
        vhat[0*ndim+k] += vhat[i*ndim+k];
      }
    }

    for(int k=0;k<ndim;k++){
      va += pow(vhat[0*ndim+k], ndim);
    }
    va = sqrt(va);
    // print order param
    cout<<va/nmax<<endl;
  }else if(plot_type == "vectorfield"){
    // print vector field data
    // only works in 2d
    for(int i=0;i<nmax;i++){
      cout << pos[i*ndim] <<" "<< pos[i*ndim+1] <<" "<< vhat[i*ndim] <<" "<< vhat[i*ndim+1] << endl;
    }
  }

}

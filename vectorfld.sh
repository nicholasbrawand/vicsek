# run vic model
# ./main nmax, tmax, rcut, eta, L, order param
make -s clean 
make -s
echo "#x #y #vx #vy">vecf.dat
./main 500   100    4.0   0.01  30 vectorfield >> vecf.dat
gnuplot -e 'set term png; set output "vecf.png"; plot "vecf.dat" using 1:2:3:4 with vectors head filled lt 2'
open vecf.png

#include <iostream>
#include "utils.h"

using namespace std;

void print_arr(double arr[], const int len1, const int len2){
  for(int i=0; i<len1; i++){
    for(int j=0; j<len2; j++){
      cout << arr[i*len2+j]<<", ";
    }
    cout << endl;
  }
}

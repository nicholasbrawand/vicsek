# run vic model
# ./main nmax, tmax, rcut, eta, L, order param
make -s clean 
make -s
echo "#eta #va">noise.dat
for i in $(seq 0.1 0.1 12)
do
  #          nmax, tmax, rcut, eta, L
  OP=`./main 100   20    0.5   0.01  $i orderparam`
  echo $i $OP
done >> noise.dat
gnuplot -e 'set term png; set output "plot.png"; plot "noise.dat" using 1:2'
open plot.png

all: main.cpp
	g++ -c -std=c++11 main.cpp print_arr.cpp -fopenmp
	g++ -o main main.o print_arr.o -fopenmp

clean: 
	$(RM) *.o main
